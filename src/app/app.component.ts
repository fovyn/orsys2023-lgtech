import {ApplicationRef, Component} from '@angular/core';
import {Store} from "@ngrx/store";
import {IState} from "./app.state";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'orsys2023-lgtech';

  constructor(
    private $store: Store<IState>
  ) {
    // $store.subscribe(state => console.log(state))
  }

  changePagination() {
    // this.$store.dispatch(load({page: 2, size: 42}))
  }
}
