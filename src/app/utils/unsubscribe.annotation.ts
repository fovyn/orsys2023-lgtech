import {Subscription} from "rxjs";

export function Unsubscribe(): ClassDecorator {
  return (constructor) => {

    const origin = constructor.prototype.ngOnDestroy
    constructor.prototype.ngOnDestroy = function() {
      for(let attrKey in constructor.prototype) {
        const attr = constructor.prototype[attrKey]
        if (attr instanceof Subscription) {
          attr.unsubscribe()
        }
      }

      origin?.invoke()
    }
  }
}
