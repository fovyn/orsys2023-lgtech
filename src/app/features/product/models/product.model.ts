export interface Product {
  id: number
  title: string
  desc: string
}

export type Pagination = {
  total: number
  page: number
  nb: number
}

export type Products =  { data: Product[] } & Pagination
