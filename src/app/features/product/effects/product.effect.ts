import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import { ProductActions } from '../actions'
import {exhaustMap, of, switchMap, tap} from "rxjs";
import {Store} from "@ngrx/store";

import * as productState from '../product.state'
import {ProductService} from "../services/product.service";
import {IState} from "../../../app.state";
import {selectItems} from "../product.state";
@Injectable()
export class ProductEffect {
  private state: productState.IState | undefined

  next$ = createEffect(() => this.$actions.pipe(
    ofType(ProductActions.next),
    exhaustMap(() => {
      const {history, pagination} = this.state!
      const data = history.find(it => it.pagination.page === pagination.page + 1)

      const p = {...pagination}
      p.page++

      if (data) {
        return of(ProductActions.requestSuccess({items: data.items, pagination: p}))
      }

      return this.$product.getAll(pagination.page + 1).pipe(
        switchMap((data) => of(ProductActions.requestSuccess({items: data, pagination: p})))
      )
    })
  ))

  // requestSuccess$ = createEffect(() => this.$actions.pipe(
  //   ofType(ProductActions.requestSuccess),
  //   exhaustMap((props) => of(ProductActions.addHistory(props.items)))
  // ))

  constructor(
    private $actions: Actions,
    private $store: Store<IState>,
    private $product: ProductService
  ) {
    $store.select(state => state.products).subscribe(state => this.state = state)
  }
}
