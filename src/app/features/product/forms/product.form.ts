import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TypedFormGroup} from "../../../shared/typed-form/typed-group";

export type FProductCreate = {title: string, desc: string}
export function fProductCreate(value?: Partial<FProductCreate>): TypedFormGroup<FProductCreate> {
  return new TypedFormGroup<FProductCreate>({
    title: new FormControl(value?.title || '', [Validators.required]),
    desc: new FormControl(value?.desc || '', [Validators.required])
  })
}

// export const FProductCreate = {
//   title: [],
//   desc: []
// }
