import {Component, Input} from '@angular/core';
import {IState, selectItems} from "../../product.state";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import { ProductActions } from '../../actions'
import * as app from '../../../../app.state'

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent {
  ProductActions = ProductActions
  // private products: any[] = []
  //
  // @Input("products") set Products(v: any[]) { this.products = [...v] }
  // get Products(): any[] { return this.products }
  //
  //
  // trackByTitle(index: number, item: any): any {
  //   return item.title
  // }

  products$: Observable<any[]> = this.$store.select((state) => selectItems(state.products))

  constructor(public $store: Store<app.IState>) {
    $store.dispatch(ProductActions.next())
  }

}
