import {Component, EventEmitter, Output} from '@angular/core';
import {fProductCreate} from "../../forms/product.form";
import {IState} from "../../../../app.state";
import {Store} from "@ngrx/store";
import {addItem} from "../../actions/product.action";
import {FormArray, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent {
  @Output('create') createEvent = new EventEmitter<any>()

  fCreate = fProductCreate()

  fArray = new FormGroup({
    name: new FormControl(),
    address: new FormArray([])
  })

  get fa(): FormArray { return this.fArray.get('address') as FormArray }

  constructor(private $store: Store<IState>) {
    $store.select((state) => state.products).subscribe({
      next: () => {},
      error: (err) => {}
    })
  }

  ngOnInit() {
    this.fCreate.patchValue({title: 'Blop'})
  }

  handleSubmit() {
    console.log(this.fCreate.value)
    if (this.fCreate.invalid) {
      return;
    }
    // this.createEvent.emit({...this.fCreate.value})
    // this.$store.dispatch(addItem(this.fCreate.value))
  }

  addAddress() {
    this.fa.push(new FormGroup({street: new FormControl()}))
  }

  removeAddress(i: number) {
    this.fa.removeAt(i)
  }
}
