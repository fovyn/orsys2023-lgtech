import {InjectionToken, ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import {ProductService} from "./services/product.service";
import { ProductCreateComponent } from './components/product-create/product-create.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SecurityModule} from "../../shared/security/security.module";
import {EffectsModule} from "@ngrx/effects";
import {ProductEffect} from "./effects/product.effect";
import {StoreModule} from "@ngrx/store";
import {reducers} from "./product.state";
import {TypedFormModule} from "../../shared/typed-form/typed-form.module";


export const apiTokenInjector = new InjectionToken("PRODUCT_API_TARGET")

export interface ProductOptionsModule {
  api: string
}

@NgModule({
  declarations: [
    ProductComponent,
    ProductListComponent,
    ProductCreateComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    TypedFormModule,
    SecurityModule.forChild(),
    // StoreModule.forFeature("products", reducers),
    EffectsModule.forFeature([ProductEffect])
    // SecurityModule
  ],
  providers: [
    ProductService
  ]
})
export class ProductModule {

}
