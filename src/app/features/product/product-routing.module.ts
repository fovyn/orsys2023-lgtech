import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product.component';
import {ProductCreateComponent} from "./components/product-create/product-create.component";

const routes: Routes = [
  { path: '', component: ProductComponent, children: [
      { path: 'add', component: ProductCreateComponent }
      // { path: 'update/{id}', component: }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
