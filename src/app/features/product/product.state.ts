import {Pagination, Product} from "./models";
import {ProductActions} from './actions'
import {createReducer, on} from "@ngrx/store";

export type ProductHistory = Array<IState & { time: number }>

export interface IState {
  pagination: Pagination
  items: Product[],
  history: ProductHistory
}

export const initialState: IState = {
  pagination: {page: 0, nb: 0, total: 0},
  items: [],
  history: []
}

export const reducers = createReducer(
  initialState,
  on(ProductActions.requestSuccess, (state, {items, pagination}) => ({...state, items, pagination }))
)

export const selectPagination = (state: IState) => state.pagination
export const selectItems = (state: IState) => state.items
export const selectHistory = (state: IState) => state.history

