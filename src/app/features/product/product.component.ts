import {AfterViewInit, Component, inject, Inject, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {apiTokenInjector} from "./product.module";
import {ProductService} from "./services/product.service";
import {Subscription} from "rxjs";
import {Unsubscribe} from "../../utils/unsubscribe.annotation";
import {ProductCreateComponent} from "./components/product-create/product-create.component";
import {AuthService} from "../../shared/security/services/auth.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
@Unsubscribe()
export class ProductComponent implements OnInit, AfterViewInit {
  // @ViewChild(ProductCreateComponent, {static: false}) createComponent: ProductCreateComponent | undefined

  private products: any[] = [];

  products$$: Subscription | undefined

  get Products(): any[] {
    return [...this.products]
  }

  constructor(
    private $products: ProductService,
    private $auth: AuthService
  ) {
  }

  ngOnInit() {
    this.products$$ = this.$products
      .getAll()
      .subscribe(data => this.products = [...data])
  }

  ngAfterViewInit() {
    // console.log(this.createComponent)

    // this.createComponent?.createEvent
  }
  ngAfterContentInit() {
    // console.log(this.createComponent)
  }

  handleProductCreate($event: any) {
    // const copy = [...this.products]
    // copy.push($event)
    // this.products = copy

    // this.products = [...this.products, $event]

    this.$products.add($event).subscribe(data => this.products = [...this.products, data])
  }
}
