import {createAction, props} from "@ngrx/store";
import {Pagination, Product} from "../models";

// Effect
export const next = createAction(
  "[Product] - next",
)
// Effect
export const previous = createAction(
  "[Product] - previous",
)

//State
export const requestFailed = createAction(
  "[Product] - failed",
    props<{err: any}>()
)
//State
export const requestSuccess = createAction(
  "[Product] - success",
  props<{items: Product[], pagination: Pagination}>()
)

//Effect
export const addItem = createAction(
  "[Product] - add",
  props<{item: Product}>()
)

//State
export const addItemSuccess = createAction(
  "[Product] - addSuccess",
  props<{item: Product}>()
)
