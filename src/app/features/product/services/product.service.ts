import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

const { base_uri } = environment

@Injectable()
export class ProductService {

  constructor(
    private $http: HttpClient
  ) { }

  getAll(page: number = 1, size: number = 5): Observable<any[]> {
    const params = new HttpParams().appendAll({_page: page, _limit: size})

    const headers = new HttpHeaders({'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImZsYXZpYW4ub3Z5bkBic3Rvcm0uYmUiLCJpYXQiOjE2ODQ5MTE5NDQsImV4cCI6MTY4NDkxNTU0NCwic3ViIjoiMSJ9.ph7hgODIgb8HgcQ-xPYJFR2woBLnbdn1VOuDQ9eNCh0`})
    return this.$http
      .get<any[]>(`${base_uri}/products`, {headers, params})
      .pipe()
  }

  add(item: any): Observable<any> {
    return this.$http.post(`${base_uri}/products`, item)
  }
}
