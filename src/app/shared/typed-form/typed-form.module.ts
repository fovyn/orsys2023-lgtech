import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { FormInputComponent } from './form-input/form-input.component';



@NgModule({
  declarations: [
    FormInputComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [FormsModule, ReactiveFormsModule, FormInputComponent]
})
export class TypedFormModule { }
