import {FormArray, FormControl, FormGroup} from "@angular/forms";

export class TypedFormGroup<T> extends FormGroup {

  getControl(key: keyof T): FormControl {
    const control = this.get(key.toString())
    if(!(control instanceof FormControl)) throw new Error()

    return control as FormControl
  }
}
