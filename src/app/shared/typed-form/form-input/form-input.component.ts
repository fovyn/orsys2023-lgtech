import {Component, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

export type InputType = 'text' | 'number' | 'date' | 'datetime'

@Component({
  selector: 'form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: FormInputComponent, multi: true }
  ]
})
export class FormInputComponent implements ControlValueAccessor {
  private label: string = ''
  private placeholder: string = ''
  private disabled: boolean = false
  private type: InputType = 'text'

  @Input('label') set Label(v: string) { this.label = v }
  @Input('placeholder') set Placeholder(v: string) { this.placeholder = v }
  @Input('disabled') set Disabled(v: boolean) { this.disabled = v }
  @Input('type') set Type(v: InputType) { this.type = v }

  get Type(): InputType { return this.type }
  get Disabled(): boolean { return this.disabled }
  get Placeholder(): string { return this.placeholder }
  get Label(): string { return this.label }
  get For(): string { return `FOR-${this.Label}`}

  value: any


  onChangeFn: any | undefined
  onTouchedFn: any | undefined

  registerOnChange(fn: any): void {
    this.onChangeFn = fn
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn
  }

  setDisabledState(isDisabled: boolean): void {
    this.Disabled = isDisabled
  }

  writeValue(obj: any): void {
    this.value = obj
  }

  handleInput({value}: any) {
    this.onChangeFn(value)
  }
}
