import {createReducer, on} from "@ngrx/store";
import {SessionActions} from "./actions/session.action";

export interface IState {
  accessToken: string,
  err: any | null
}

export const initialState: IState = {
  accessToken: '',
  err: null
}

export const reducers = createReducer(
  initialState,
  on(SessionActions.loginSuccess, (state, {accessToken}) => ({...state, accessToken})),
  on(SessionActions.loginFailed, (state, {err}) => ({...state, err}))
)
