// export type TUser = {
//   accessToken: string
//   user: any
// }

export interface IUser {
  id?: number
  email: string
}

export interface ICredential {
  email: string
  password: string
}

export type TUser = { accessToken: string } & { user: any }
