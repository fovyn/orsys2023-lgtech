import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {SessionActions} from "../actions/session.action";
import {catchError, exhaustMap, map, of, switchMap, tap} from "rxjs";
import {AuthService} from "../services/auth.service";
import {IUser, TUser} from "../models/user.model";
import {Router} from "@angular/router";
import {ErrorActions} from "../../../app.state";
import {SecurityModule} from "../security.module";

@Injectable()
export class SessionEffect {

  login$ = createEffect(() => this.$actions.pipe(
    ofType(SessionActions.login),
    exhaustMap((data) => this.$auth.signIn(data.email, data.password).pipe(
      switchMap(({accessToken, user}) => of(SessionActions.loginSuccess({accessToken}))),
      catchError((err) => of(ErrorActions.trigger({module: SecurityModule, data: err})))
    ))
  ))

  loginSuccess$ = createEffect(() => this.$actions.pipe(
    ofType(SessionActions.loginSuccess),
    exhaustMap((data) => of(this.$router.navigate([''])))
  ), {dispatch: false})

  constructor(private $actions: Actions, private $auth: AuthService, private $router: Router) {
  }
}
