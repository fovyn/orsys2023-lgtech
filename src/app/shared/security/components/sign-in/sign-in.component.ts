import { Component } from '@angular/core';
import {Unsubscribe} from "../../../../utils/unsubscribe.annotation";
import {filter, map, Subscription} from "rxjs";
import {AuthService} from "../../services/auth.service";
import {IUser} from "../../models/user.model";
import {HttpHeaders} from "@angular/common/http";
import {Store} from "@ngrx/store";
import {SessionActions} from "../../actions/session.action";
import {ErrorActions, IState} from "../../../../app.state";
import {SecurityModule} from "../../security.module";

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
@Unsubscribe()
export class SignInComponent {

  signIn$$: Subscription | undefined

  constructor(
    private $store: Store<IState>
  ) {
    // $store.select((state) => state.error)
    //   .pipe(
    //     filter(it => it.module === SecurityModule)
    //   )
    //   .subscribe((err) => {
    //     console.log(err)
    //     // $store.dispatch(ErrorActions.clear())
    //   })
    $store.dispatch(SessionActions.login({email: 'flavian.ovyn@bstorm.be', password: 'Test13='}))
  }

  ngOnDestroy() {

  }
}
