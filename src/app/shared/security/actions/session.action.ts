import {createAction, createReducer, props} from "@ngrx/store";
import {ICredential} from "../models/user.model";

const login = createAction(
  "[Session] - login",
  props<ICredential>()
)

const loginSuccess = createAction(
  "[Session] - login success",
  props<{accessToken: string}>()
)

const loginFailed = createAction(
  "[Session] - login failed",
  props<{err: any}>()
)

export const SessionActions = {login, loginSuccess, loginFailed}
