import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable, of, switchMap, take, tap} from "rxjs";
import {TUser} from "../models/user.model";
import {SessionService} from "./session.service";
import {TOKEN_REGISTER, TOKEN_SIGN_IN} from "../security.module";

@Injectable()
export class AuthService {

  constructor(
    @Inject(TOKEN_SIGN_IN) private $uriSignIn: string,
    @Inject(TOKEN_REGISTER) private $uriRegister: string,
    private $http: HttpClient,
    private $session: SessionService
  ) {
  }

  signIn(email: string, password: string): Observable<TUser> {
    return this.$http
      .post<TUser>(`${this.$uriSignIn}`, {email, password})
      .pipe(
      )
  }

  register<T>(user: T): Observable<TUser> {
    return this.$http
      .post<TUser>(`${this.$uriRegister}`, user)
      .pipe()
  }
}
