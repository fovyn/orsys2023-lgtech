import {Injectable, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from "rxjs";
import {IUser} from "../models/user.model";
import {Unsubscribe} from "../../../utils/unsubscribe.annotation";

@Injectable()
@Unsubscribe()
export class SessionService implements OnInit {
  private sessions$ = new BehaviorSubject<string | null>(null)

  get Token$(): Observable<string | null> {
    return this.sessions$.asObservable()
  }
  get Token(): string | null { return this.sessions$.value }


  constructor() {
  }

  ngOnInit() {
    const token = localStorage.getItem("TOKEN")
    this.sessions$.next(token)
  }

  login(token: string) {
    this.sessions$.next(token)
    localStorage.setItem("TOKEN", token)
  }
  logout() {
    this.sessions$.next(null)
    localStorage.removeItem("TOKEN")
  }
}
