import {InjectionToken, ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './components/sign-in/sign-in.component';
import {SessionService} from "./services/session.service";
import {AuthService} from "./services/auth.service";
import {EffectsModule} from "@ngrx/effects";
import {SessionEffect} from "./effects/session.effect";
import {StoreModule} from "@ngrx/store";
import {reducers} from "./security.state";


export const TOKEN_REGISTER = new InjectionToken("security.register")
export const TOKEN_SIGN_IN = new InjectionToken("security.sign_in")


@NgModule({
  declarations: [
    SignInComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature("security", reducers),
    EffectsModule.forFeature([SessionEffect])
  ],
  exports: [
    SignInComponent
  ],
  providers: [AuthService]
})
export class SecurityModule {
  static forRoot(options: {registerEndpoint: string, signInEndpoint: string}): ModuleWithProviders<SecurityModule> {
    const { registerEndpoint, signInEndpoint } = options
    return {
      ngModule: SecurityModule,
      providers: [
        {provide: TOKEN_REGISTER, useValue: registerEndpoint},
        {provide: TOKEN_SIGN_IN, useValue: signInEndpoint},
        SessionService
      ]
    }
  }
  static forChild(): ModuleWithProviders<SecurityModule> {
    return {
      ngModule: SecurityModule,
      providers: []
    }
  }
}
