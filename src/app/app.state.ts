import * as productState from './features/product/product.state'
import * as securityState from './shared/security/security.state'
import {createAction, createReducer, on, props} from "@ngrx/store";
import {of} from "rxjs";

export type ErrorState = { module: any, data: any }
export interface IState {
  products: productState.IState
  error: ErrorState
  security: securityState.IState
}

export const ErrorActions: any = {
  trigger: createAction('[Error] - trigger', props<ErrorState>()),
  clear: createAction('[Error] - clear')
}

const errorReducer = createReducer(
  {module: null, data: null},
  on(ErrorActions.trigger, (state, {module, data}) => ({module, data})),
  on(ErrorActions.clear, (state) => ({module: null, data: null}))
)

export const reducers = {
  products: productState.reducers,
  error: errorReducer,
  security: securityState.reducers,
}

export const selectProducts = (state: IState) => state.products
// export const reducers: IState = {
//   products:
// }
