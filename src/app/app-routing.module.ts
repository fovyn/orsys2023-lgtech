import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SignInComponent} from "./shared/security/components/sign-in/sign-in.component";

const routes: Routes = [
  { path: 'login', component: SignInComponent },
  { path: 'products', loadChildren: () => import('./features').then(m => m.ProductModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
